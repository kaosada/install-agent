echo "Installing epel repository"
yum install -y epel-release

echo "Running: yum install -y nagios-okconfig-nrpe"
yum install -y nagios-okconfig-nrpe nagios nagios-plugins-all nrpe wget git

#Install Telegraf
cd /tmp/
wget https://dl.influxdata.com/telegraf/releases/telegraf-1.14.1-1.x86_64.rpm &&
yum -y localinstall telegraf-1.14.1-1.x86_64.rpm


# Chmod File
#chmod +x check*
#Copy File 
cp /tmp/install-agent/check_oom /usr/lib64/nagios/plugins/
cp /tmp/install-agent/check_cpu /usr/lib64/nagios/plugins/

echo "Done"
